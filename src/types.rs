use std::{
  collections::{hash_map::DefaultHasher, HashMap},
  fmt::Display,
  hash::{Hash, Hasher},
};

use crate::env::Env;

#[derive(Debug, Clone, Default, Hash)]
pub struct Location {
  pub filename: Option<String>,
  pub line: usize,
  pub column: usize,
}

impl Display for Location {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    if self.filename.is_some() {
      write!(
        f,
        "{}:{}:{}",
        self.filename.as_ref().unwrap(),
        self.line,
        self.column
      )
    } else {
      write!(f, "{}:{}", self.line, self.column)
    }
  }
}

#[derive(Debug, Clone)]
pub struct Token {
  pub location: Option<Location>,
  pub value: TokenType,
}

#[derive(Debug, Clone)]
pub enum TokenType {
  Nil,
  Number(f64),
  String(String),
  Symbol(String),
  Keyword(String),
  Boolean(bool),

  List(Box<Vec<Token>>),
  HashMap(Box<HashMap<Token, Token>>),

  Function {
    arity: Option<usize>,
    executor: fn(Vec<Token>) -> Result<Token, AosetiError>,
  },
  Lambda {
    bindings: Box<Vec<Token>>,
    env: Box<Env>,
    arity: Option<usize>,
    body: Box<Token>,
  },
}

#[derive(Debug)]
pub struct AosetiError {}

// Implementations
impl Display for Token {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match &self.value {
      TokenType::Nil => write!(f, "nil"),
      TokenType::Number(value) => write!(f, "{}", value),
      TokenType::String(value) => write!(f, "\"{}\"", value),
      TokenType::Symbol(value) => write!(f, "{}", value),
      TokenType::Keyword(value) => write!(f, "{}", value),
      TokenType::Boolean(value) => write!(f, "{}", value),
      TokenType::List(collection) => {
        if collection.len() == 0 {
          return write!(f, "()");
        }

        let mut str = "(".to_string();
        collection
          .iter()
          .for_each(|it| str += format!("{} ", it).as_str());

        str = str.get(0..str.len() - 2).unwrap().to_string();
        str += ")";
        write!(f, "{}", str)
      }
      TokenType::HashMap(_) => todo!(),
      TokenType::Function { arity, .. } => {
        let mut hasher = DefaultHasher::new();
        self.value.hash(&mut hasher);
        let hashed = hasher.finish();
        write!(
          f,
          "<#function({}) {:?}>",
          if arity.is_some() {
            format!("{}", arity.unwrap())
          } else {
            "∞".to_string()
          },
          hashed
        )
      }
      TokenType::Lambda { arity, .. } => {
        let mut hasher = DefaultHasher::new();
        self.value.hash(&mut hasher);
        let hashed = hasher.finish();
        write!(
          f,
          "<#function({}) {:?}>",
          if arity.is_some() {
            format!("{}", arity.unwrap())
          } else {
            "∞".to_string()
          },
          hashed
        )
      }
    }
  }
}

impl Hash for Token {
  fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
    self.location.hash(state);
    self.value.hash(state);
  }
}

impl Hash for TokenType {
  fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
    match self {
      TokenType::Nil => NIL_HASH_TOKEN.hash(state),
      TokenType::Number(_) => todo!(),
      TokenType::String(value) => value.clone().hash(state),
      TokenType::Symbol(value) => format!("{}{}", SYMBOL_HASH_PREFIX, value).hash(state),
      TokenType::Keyword(value) => format!("{}{}", KEYWORD_HASH_PREFIX, value).hash(state),
      TokenType::Boolean(value) => value.clone().hash(state),
      TokenType::List(collection) => {
        for item in collection.iter() {
          item.hash(state)
        }
      }
      TokenType::HashMap(collection) => {
        for item in collection.iter() {
          item.hash(state)
        }
      }
      TokenType::Function { arity, executor } => {
        arity.hash(state);
        executor.hash(state);
      }
      TokenType::Lambda {
        bindings,
        arity,
        body,
        env,
      } => {
        bindings.hash(state);
        arity.hash(state);
        body.hash(state);
        env.hash(state);
      }
    }
  }
}

impl Token {
  /* #region Lists */
  pub fn is_empty(&self) -> bool {
    match &self.value {
      TokenType::List(collection) => collection.is_empty(),
      _ => panic!("Can`t call is_empty on {}", self),
    }
  }

  pub fn get_head(&self) -> Option<&Token> {
    match &self.value {
      TokenType::List(collection) => collection.first(),
      _ => panic!("Can`t call get_head on {}", self),
    }
  }

  pub fn get_rest(&self) -> Vec<Token> {
    match &self.value {
      TokenType::List(collection) => collection.get(1..collection.len()).unwrap().to_vec(),
      _ => panic!("Can`t call get_head on {}", self),
    }
  }

  pub fn is_list(&self) -> bool {
    match &self.value {
      TokenType::List(..) => true,
      _ => false,
    }
  }

  pub fn as_list(&self) -> &Box<Vec<Token>> {
    match &self.value {
      TokenType::List(collection) => collection,
      _ => panic!("{} is not a list", self),
    }
  }
  /*  #endregion */

  /* #region Functions */
  pub fn is_function(&self) -> bool {
    match self.value {
      TokenType::Function { .. } => true,
      _ => false,
    }
  }

  pub fn is_lambda(&self) -> bool {
    match self.value {
      TokenType::Lambda { .. } => true,
      _ => false,
    }
  }

  pub fn apply_function(
    &self,
    original_symbol: Token,
    arguments: Vec<Token>,
  ) -> Result<Token, AosetiError> {
    match self.value {
      TokenType::Function { arity, executor } => {
        if arity.is_none() || arity.unwrap() == arguments.len() {
          return executor(arguments);
        }
        panic!(
          "Function {} expects {} arguments. Received {}",
          original_symbol,
          arity.unwrap(),
          arguments.len()
        )
      }
      _ => panic!("{} is not callable", original_symbol),
    }
  }

  pub fn apply_lambda(&self, original_symbol: Token, arguments: Vec<Token>) -> (Token, Env) {
    match &self.value {
      TokenType::Lambda {
        bindings,
        env,
        arity,
        body,
      } => {
        if arity.is_none() || arity.unwrap() == arguments.len() {
          let mut new_env = Env::default();
          new_env.set_parent(env.as_ref());

          for i in 0..bindings.len() {
            let key = bindings[i].clone();
            let value = arguments[i].clone();
            new_env.set(key.as_symbol(), value);
          }

          return (body.as_ref().clone(), new_env);
        }
        panic!(
          "Function {} expects {} arguments. Received {}",
          original_symbol,
          arity.unwrap(),
          arguments.len()
        )
      }
      _ => panic!("{} is not callable", original_symbol),
    }
  }
  /*  #endregion */

  /* #region Math */
  pub fn add(&self, other: &Token) -> Result<Token, AosetiError> {
    match &self.value {
      TokenType::Number(a) => match &other.value {
        TokenType::Number(b) => {
          return Ok(Token {
            location: self.location.clone(),
            value: TokenType::Number(a + b),
          })
        }
        _ => Err(AosetiError {}),
      },
      _ => Err(AosetiError {}),
    }
  }
  /*  #endregion */

  /* #region Symbols */
  pub fn is_symbol(&self) -> bool {
    match self.value {
      TokenType::Symbol(..) => true,
      _ => false,
    }
  }

  pub fn as_symbol(&self) -> String {
    match &self.value {
      TokenType::Symbol(value) => value.clone(),
      _ => panic!("{} is not a Symbol", self),
    }
  }
  /*  #endregion */

  /* #region Truthynss */
  pub fn is_truthy(&self) -> bool {
    match &self.value {
      TokenType::Nil => false,
      TokenType::Boolean(value) => *value,
      _ => true,
    }
  }
  /*  #endregion */
}

const NIL_HASH_TOKEN: &str = "2aca83ee-79f8-44c8-aeeb-a2ffa1904ff8";
const SYMBOL_HASH_PREFIX: &str = "1d5bf3d2-6af5-4987-950f-5cebeb6604f4";
const KEYWORD_HASH_PREFIX: &str = "4388abe9-8f0e-41d4-92e4-2b3c0b4fdd5a";
