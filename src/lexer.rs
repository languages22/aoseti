use std::fmt::Display;

use crate::{string_reader::StringReader, types::Location};
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug, Clone)]
pub enum LexerTokenType {
  ListStart,
  ListEnd,
  Nil,
  True,
  False,
  DecimalNumber(String),
  Symbol(String),
  Keyword(String),
  String(String),
}

impl Display for LexerTokenType {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self {
      LexerTokenType::ListStart => write!(f, "("),
      LexerTokenType::ListEnd => write!(f, ")"),
      LexerTokenType::Nil => write!(f, "nil"),
      LexerTokenType::True => write!(f, "true"),
      LexerTokenType::False => write!(f, "false"),
      LexerTokenType::DecimalNumber(value) => write!(f, "{}", &value),
      LexerTokenType::Symbol(value) => write!(f, "{}", &value),
      LexerTokenType::Keyword(value) => write!(f, "{}", &value),
      LexerTokenType::String(value) => write!(f, "{}", &value),
    }
  }
}

#[derive(Debug, Clone)]
pub struct LexerToken {
  pub location: Location,
  pub value: LexerTokenType,
}

impl LexerToken {
  pub fn is_list_end(&self) -> bool {
    match &self.value {
      LexerTokenType::ListEnd => true,
      _ => false,
    }
  }
}

pub fn skip_whitespace(reader: &mut StringReader) {
  lazy_static! {
    static ref WSRE: Regex = Regex::new(r"[\s,]").unwrap();
  }
  while !reader.is_empty() && WSRE.is_match(reader.peek(1).as_str()) {
    reader.grab(1);
  }
}

pub fn read_fixed_string(
  reader: &mut StringReader,
  string: &str,
  f: fn(Location) -> LexerToken,
) -> Option<LexerToken> {
  if reader.peek(string.len()) == string {
    let result = Some(f(reader.get_current_location()));
    reader.grab(string.len());
    result
  } else {
    None
  }
}

pub fn read_nil(reader: &mut StringReader) -> Option<LexerToken> {
  read_fixed_string(reader, "nil", |location| LexerToken {
    location,
    value: LexerTokenType::Nil,
  })
}

pub fn read_list_start(reader: &mut StringReader) -> Option<LexerToken> {
  read_fixed_string(reader, "(", |location| LexerToken {
    location,
    value: LexerTokenType::ListStart,
  })
}

pub fn read_list_end(reader: &mut StringReader) -> Option<LexerToken> {
  read_fixed_string(reader, ")", |location| LexerToken {
    location,
    value: LexerTokenType::ListEnd,
  })
}

pub fn read_true(reader: &mut StringReader) -> Option<LexerToken> {
  read_fixed_string(reader, "true", |location| LexerToken {
    location,
    value: LexerTokenType::True,
  })
}

pub fn read_false(reader: &mut StringReader) -> Option<LexerToken> {
  read_fixed_string(reader, "false", |location| LexerToken {
    location,
    value: LexerTokenType::False,
  })
}

pub fn read_decimal_numner(reader: &mut StringReader) -> Option<LexerToken> {
  lazy_static! {
    static ref IS_NUMBER: Regex = Regex::new(r"\d").unwrap();
    static ref IS_SIGNED_NUMBER: Regex = Regex::new(r"[+-]\d").unwrap();
    static ref VALID_CHAR: Regex = Regex::new(r"^[-+\d.]+$").unwrap();
    static ref CHECK: Regex = Regex::new(r"^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$").unwrap();
  }
  if IS_NUMBER.is_match(reader.peek(1).as_str())
    || IS_SIGNED_NUMBER.is_match(reader.peek(2).as_str())
  {
    let location = reader.get_current_location();
    let mut peeked = 1;
    while VALID_CHAR.is_match(reader.peek(peeked).as_str()) && !reader.is_empty_with_offset(peeked)
    {
      peeked += 1;
    }
    if !reader.is_empty_with_offset(peeked) || reader.peek(peeked).ends_with(")") {
      peeked -= 1;
    }

    return if CHECK.is_match(reader.peek(peeked).as_str()) {
      Some(LexerToken {
        location,
        value: LexerTokenType::DecimalNumber(reader.grab(peeked)),
      })
    } else {
      None
    };
  }
  None
}

pub fn read_string(reader: &mut StringReader) -> Option<LexerToken> {
  if reader.peek(1) != "\"" {
    return None;
  }
  let location = reader.get_current_location();
  let mut str = "".to_string();
  reader.grab(1);

  lazy_static! {
    static ref STRING_TERM: Regex = Regex::new(r#"[^\\]""#).unwrap();
  }

  while !reader.is_empty() && !STRING_TERM.is_match(reader.peek(2).as_str()) {
    str += reader.grab(1).as_str();
  }
  str += reader.grab(1).as_str();
  if reader.is_empty() {
    panic!("Unfinished string at {}", location);
  }

  reader.grab(1);
  Some(LexerToken {
    location,
    value: LexerTokenType::String(str),
  })
}

pub fn read_keyword(reader: &mut StringReader) -> Option<LexerToken> {
  lazy_static! {
    static ref KW_REGEX: Regex = Regex::new(r"^[-:\d\w$/*&#!]+$").unwrap();
    static ref KW_CHECK: Regex = Regex::new(r"^::?[-\d\w$/\\*&#!]+$").unwrap();
  }
  if reader.peek(1) == ":" {
    let mut peeked = 1;
    let location = reader.get_current_location();

    while !reader.is_empty_with_offset(peeked) && KW_REGEX.is_match(reader.peek(peeked).as_str()) {
      peeked += 1;
    }
    if !reader.is_empty_with_offset(peeked) {
      peeked -= 1;
    }
    return if KW_CHECK.is_match(reader.peek(peeked).as_str()) {
      Some(LexerToken {
        location,
        value: LexerTokenType::Keyword(reader.grab(peeked)),
      })
    } else {
      None
    };
  }
  None
}

pub fn read_symbol(reader: &mut StringReader) -> Option<LexerToken> {
  lazy_static! {
    static ref SYM_REGEX: Regex = Regex::new(r"^[-+\d\w$/\\*&#!]+$").unwrap();
    static ref SYM_CHECK: Regex = Regex::new(r"^[-+\d\w$/\\*&#!]+$").unwrap();
  }

  let mut peeked = 1;
  let location = reader.get_current_location();

  while !reader.is_empty_with_offset(peeked) && SYM_REGEX.is_match(reader.peek(peeked).as_str()) {
    peeked += 1;
  }
  if !reader.is_empty_with_offset(peeked) {
    peeked -= 1;
  }

  return if SYM_CHECK.is_match(reader.peek(peeked).as_str()) {
    Some(LexerToken {
      location,
      value: LexerTokenType::Symbol(reader.grab(peeked)),
    })
  } else {
    None
  };
}

pub fn lex_string(input: String, filename: Option<String>) -> Vec<LexerToken> {
  let mut reader = StringReader::new(input, filename);
  let mut result: Vec<LexerToken> = vec![];

  skip_whitespace(&mut reader);

  while !reader.is_empty() {
    skip_whitespace(&mut reader);
    if reader.is_empty() {
      break;
    }

    if let Some(token) = read_nil(&mut reader) {
      result.push(token);
      continue;
    }
    if let Some(token) = read_list_start(&mut reader) {
      result.push(token);
      continue;
    }
    if let Some(token) = read_list_end(&mut reader) {
      result.push(token);
      continue;
    }
    if let Some(token) = read_true(&mut reader) {
      result.push(token);
      continue;
    }
    if let Some(token) = read_false(&mut reader) {
      result.push(token);
      continue;
    }
    if let Some(token) = read_decimal_numner(&mut reader) {
      result.push(token);
      continue;
    }
    if let Some(token) = read_keyword(&mut reader) {
      result.push(token);
      continue;
    }
    if let Some(token) = read_string(&mut reader) {
      result.push(token);
      continue;
    }
    if let Some(token) = read_symbol(&mut reader) {
      result.push(token);
      continue;
    }

    println!("Parsed: {:#?}", result);
    panic!(
      "Unredable token {} at {}",
      reader.peek(1),
      reader.get_current_location()
    );
  }

  result
}
