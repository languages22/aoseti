use crate::{
  lexer::{LexerToken, LexerTokenType},
  token_reader::TokenReader,
  types::{Token, TokenType},
};

pub fn parse_any_token(reader: &mut TokenReader) -> Token {
  let item = reader.peek().clone();

  match &item.value {
    LexerTokenType::ListStart => {
      let mut collection = vec![];
      reader.grab();

      while !reader.is_empty() && !reader.peek().is_list_end() {
        collection.push(parse_any_token(reader));
      }

      reader.grab();
      Token {
        location: Some(item.location.clone()),
        value: TokenType::List(Box::from(collection)),
      }
    }
    LexerTokenType::Nil => {
      reader.grab();
      Token {
        location: Some(item.location.clone()),
        value: TokenType::Nil,
      }
    }
    LexerTokenType::True => {
      reader.grab();
      Token {
        location: Some(item.location.clone()),
        value: TokenType::Boolean(true),
      }
    }
    LexerTokenType::False => {
      reader.grab();
      Token {
        location: Some(item.location.clone()),
        value: TokenType::Boolean(false),
      }
    }
    LexerTokenType::DecimalNumber(value) => {
      reader.grab();
      Token {
        location: Some(item.location.clone()),
        value: TokenType::Number(value.parse::<f64>().unwrap()),
      }
    }
    LexerTokenType::Symbol(value) => {
      reader.grab();
      Token {
        location: Some(item.location.clone()),
        value: TokenType::Symbol(value.clone()),
      }
    }
    LexerTokenType::Keyword(value) => {
      reader.grab();
      Token {
        location: Some(item.location.clone()),
        value: TokenType::Keyword(value.clone()),
      }
    }
    LexerTokenType::String(value) => {
      reader.grab();
      Token {
        location: Some(item.location.clone()),
        value: TokenType::String(value.clone()),
      }
    }
    _ => panic!("Unexpected token {} at {}", &item.value, &item.location),
  }
}

pub fn parse_lexer_tokens(tokens: Vec<LexerToken>) -> Token {
  let mut reader = TokenReader::new(tokens);
  let mut tokens = vec![Token {
    location: None,
    value: crate::types::TokenType::Symbol("do".to_string()),
  }];
  let mut parsed = vec![];

  while !reader.is_empty() {
    parsed.push(parse_any_token(&mut reader));
  }

  tokens.append(&mut parsed);
  Token {
    location: None,
    value: TokenType::List(Box::from(tokens)),
  }
}
