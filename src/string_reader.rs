use crate::types::Location;

pub struct StringReader {
  index: usize,
  data: String,
  filename: Option<String>,
}

impl StringReader {
  pub fn new(data: String, filename: Option<String>) -> Self {
    Self {
      index: 0,
      data,
      filename,
    }
  }

  pub fn peek(&self, count: usize) -> String {
    let end = if self.index + count >= self.data.len() {
      self.data.len()
    } else {
      self.index + count
    };

    self.data.get(self.index..end).unwrap().to_string()
  }

  pub fn grab(&mut self, count: usize) -> String {
    let start = self.index;
    let end = if self.index + count >= self.data.len() {
      self.data.len()
    } else {
      self.index + count
    };
    self.index += count;
    self.data.get(start..end).unwrap().to_string()
  }

  pub fn is_empty(&self) -> bool {
    self.index >= self.data.len()
  }

  pub fn is_empty_with_offset(&self, offset: usize) -> bool {
    self.index + offset >= self.data.len()
  }

  pub fn get_current_location(&self) -> Location {
    let mut line: usize = 0;
    let mut column: usize = 0;

    let lines = self.data.lines();
    let mut current_buffer = 0;
    for (i, line_text) in lines.enumerate() {
      if current_buffer + line_text.len() >= self.index {
        line = i;
        column = self.index - current_buffer;
        break;
      }
      current_buffer += line_text.len();
    }

    Location {
      filename: if self.filename.is_some() {
        Some(self.filename.as_ref().unwrap().clone())
      } else {
        None
      },
      line,
      column,
    }
  }
}
