use crate::lexer::LexerToken;

pub struct TokenReader {
  index: usize,
  data: Vec<LexerToken>,
}

impl TokenReader {
  pub fn new(data: Vec<LexerToken>) -> Self {
    Self { index: 0, data }
  }

  pub fn is_empty(&self) -> bool {
    self.index >= self.data.len()
  }

  pub fn peek(&self) -> &LexerToken {
    self.data.get(self.index).unwrap()
  }

  pub fn grab(&mut self) -> &LexerToken {
    let index_copy = self.index;
    self.index += 1;
    self.data.get(index_copy).unwrap()
  }
}
