use crate::{
  env::Env,
  types::{AosetiError, Token, TokenType},
};

pub fn evaluate_ast(token: &Token, env: &Env) -> Result<Token, AosetiError> {
  match &token.value {
    TokenType::Symbol(value) => {
      let result = env.get(&value);
      return if result.is_some() {
        Ok(result.unwrap().clone())
      } else {
        Err(AosetiError {})
      };
    }
    TokenType::List(collection) => {
      let mut new_collection = vec![];
      for item in collection.iter() {
        let evaluated_item = evaluate(item, &mut env.clone());
        if evaluated_item.is_err() {
          return evaluated_item;
        } else {
          new_collection.push(evaluated_item.unwrap())
        }
      }
      Ok(Token {
        location: token.location.clone(),
        value: TokenType::List(Box::from(new_collection)),
      })
    }
    _ => Ok(token.clone()),
  }
}

pub fn evaluate(token: &Token, env: &mut Env) -> Result<Token, AosetiError> {
  let mut env_copy = env.clone();
  let mut token_copy = token.clone();
  let result: Result<Token, AosetiError>;

  'tco: loop {
    match &token_copy.value {
      TokenType::List(collection) => {
        if token_copy.is_empty() {
          result = Ok(token_copy.clone());
          break 'tco;
        }

        let first_form = collection[0].clone();
        /* #region Special forms */
        match &first_form.value {
          TokenType::Symbol(value) => match value.as_str() {
            "def" => {
              if collection.len() != 3 {
                result = Err(AosetiError {});
                break 'tco;
              }
              let key = collection[1].clone();
              let value = collection[2].clone();

              if !key.is_symbol() {
                result = Err(AosetiError {});
                break 'tco;
              }

              let evaluated_value = evaluate(&value, &mut env_copy);
              if evaluated_value.is_err() {
                result = evaluated_value;
                break 'tco;
              }
              let unwrapped = evaluated_value.unwrap();
              env_copy.set(key.as_symbol(), unwrapped.clone());
              result = Ok(unwrapped);
              break 'tco;
            }
            "let" => {
              if collection.len() != 3 {
                result = Err(AosetiError {});
                break 'tco;
              }

              let bindings = collection[1].clone();
              let body = collection[2].clone();

              if !bindings.is_list() {
                result = Err(AosetiError {});
                break 'tco;
              }

              let mut new_env = Env::default();
              env.set_parent(&env_copy);

              let bindings = bindings.as_list();
              if bindings.len() % 2 != 0 {
                result = Err(AosetiError {});
                break 'tco;
              }

              for i in (0..bindings.len()).step_by(2) {
                let key = bindings[i].clone();
                let value = bindings[i + 1].clone();

                if !key.is_symbol() {
                  result = Err(AosetiError {});
                  break 'tco;
                }

                let evaluated_value = evaluate(&value, &mut new_env);
                if evaluated_value.is_err() {
                  result = evaluated_value;
                  break 'tco;
                }
                let unwrapped = evaluated_value.unwrap();
                new_env.set(key.as_symbol(), unwrapped.clone());
              }

              token_copy = body;
              env_copy = new_env;
              continue 'tco;
            }
            "do" => {
              if collection.len() == 1 {
                result = Ok(Token {
                  location: token_copy.location.clone(),
                  value: TokenType::Nil,
                });
                break 'tco;
              }

              for i in 1..collection.len() - 1 {
                let evaluated = evaluate(&collection[i].clone(), &mut env_copy);
                if evaluated.is_err() {
                  result = evaluated;
                  break 'tco;
                }
              }

              token_copy = collection[collection.len() - 1].clone();
              continue 'tco;
            }
            "if" => {
              if collection.len() != 4 {
                result = Err(AosetiError {});
                break 'tco;
              }
              let condition = collection[1].clone();
              let truthy = collection[2].clone();
              let falsy = collection[3].clone();

              let evaluated_condition = evaluate(&condition, &mut env_copy);
              if evaluated_condition.is_err() {
                result = evaluated_condition;
                break 'tco;
              }

              token_copy = if evaluated_condition.unwrap().is_truthy() {
                truthy
              } else {
                falsy
              };
              continue 'tco;
            }
            "fn" => {
              if collection.len() != 3 {
                result = Err(AosetiError {});
                break 'tco;
              }

              let bindings = collection[1].clone();
              let body = collection[2].clone();

              if !bindings.is_list() {
                result = Err(AosetiError {});
                break 'tco;
              }
              let bindings = bindings.as_list().clone();
              let arity = Some(bindings.len());

              result = Ok(Token {
                location: token_copy.location.clone(),
                value: TokenType::Lambda {
                  bindings: Box::from(bindings),
                  arity,
                  body: Box::from(body),
                  env: Box::from(env_copy.clone()),
                },
              });
              break 'tco;
            }
            _ => {}
          },
          _ => {}
        }
        /*  #endregion */

        /* #region Function calls */
        let evaluated_list = evaluate_ast(&token_copy, &env_copy);
        if evaluated_list.is_err() {
          result = evaluated_list;
          break 'tco;
        }

        let unwrapped = evaluated_list.unwrap();
        let func = unwrapped.get_head().unwrap().clone();
        let args = unwrapped.get_rest();

        if func.is_function() {
          result = func.apply_function(first_form, args);
          break 'tco;
        }

        if func.is_lambda() {
          let (the_token, the_env) = func.apply_lambda(first_form, args);
          token_copy = the_token;
          env_copy = the_env;
          continue;
        }

        /*  #endregion */
      }
      _ => {
        result = evaluate_ast(&token_copy, &env_copy);
        break 'tco;
      }
    }
  }

  env.apply_changes(env_copy);
  result
}
