use std::{collections::HashMap, hash::Hash};

use crate::types::{AosetiError, Token, TokenType};

#[derive(Default, Clone, Debug)]
pub struct Env {
  parent: Option<Box<Env>>,
  storage: Box<HashMap<String, Token>>,
}

impl Hash for Env {
  fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
    self.parent.hash(state);
    for elt in self.storage.iter() {
      elt.hash(state)
    }
  }
}

impl Env {
  pub fn apply_changes(&mut self, other: Env) {
    self.storage = other.storage
  }

  pub fn set_parent(&mut self, other: &Env) {
    self.parent = Some(Box::from(other.clone()))
  }

  pub fn get(&self, key: &String) -> Option<Token> {
    let item = self.storage.get(key);

    return if item.is_some() {
      Some(item.unwrap().clone())
    } else if self.parent.is_some() {
      self.parent.as_ref().unwrap().get(key)
    } else {
      None
    };
  }

  pub fn set(&mut self, key: String, value: Token) {
    self.storage.insert(key, value);
  }

  pub fn define_function(
    &mut self,
    key: &str,
    arity: Option<usize>,
    executor: fn(Vec<Token>) -> Result<Token, AosetiError>,
  ) {
    self.set(
      key.to_string(),
      Token {
        location: None,
        value: TokenType::Function { arity, executor },
      },
    )
  }
}
