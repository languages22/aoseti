use env::Env;
use evaluation::evaluate;
use lexer::lex_string;
use parser::parse_lexer_tokens;

mod env;
mod evaluation;
mod lexer;
mod parser;
mod string_reader;
mod token_reader;
mod types;

fn main() {
  let input = r#"(+ 34.34 35.35)"#.to_string();
  let parsed = lex_string(input, None);
  let ast = parse_lexer_tokens(parsed);

  let mut env = Env::default();
  env.define_function("+", Some(2), |args| {
    return args[0].add(&args[1]);
  });

  let result = evaluate(&ast, &mut env);

  println!("{}", result.unwrap());
}
