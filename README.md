# aoseti
## Overview
Aoseti lisp like language.
* Statically typed but dynamic in nature. Like Typescript
* Enforcing the idea of Specs (see Clojure specs)
* Interpreted
* Evrething is a function

## A gist of aoseti
```
; Comments
;* Block comment *;

; Hello World
(println "Hello, World!")

; Functons untyped
(defun add-two (a b)
    (+ a b))

; Adding specs, specs are functions too
(defun @positive (value)
    (->> value
        @number
        (>= 0)))

(defspec add-two
    :args (@number @positive)
    :return @number)

; Protocols
(defprotocol Quacker
    (spec quack
        :args ()
        :return @string))

; Named maps (records) e.g. "structs"
(defrecord Duck
    :age @number
    :name #(->> it @string (@with-length-of 3))

(defrecord PackOfDucks
    :pack  @Duck ; Records and Protocols can be used as specs
    :count @positive)

; Records are like interfaces of Typescript e.g. any map with the required attributes with correct specs is a valid Record

; This coerces to @Duck, so its a duck
({:age 2 :name "Bob" :color "white"})

; Implementing protocols
(impl-protocol Duck Quacker
    (defun quack (self) ; Every function of a protocol receives a record
    that is implementing this protocol as a first parametr
        "Quack!"))

; Calling protocol functions
(def duck (get-duck-somehow))
(quack duck)

; Gettings map fields. Maps are functions of their keys
(def sample {:x 10 :z 20})
(sample :x) ;; 10
```